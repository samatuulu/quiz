from django.contrib import admin

from webapp.models import Intro, Quiz, QuizOptions

admin.site.register(Intro)
admin.site.register(Quiz)
admin.site.register(QuizOptions)