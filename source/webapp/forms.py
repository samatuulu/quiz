from django import forms

from webapp.models import Quiz


class QuizCreateForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ['author','question', 'image', 'question_hint']
