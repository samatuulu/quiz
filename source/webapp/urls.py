from django.urls import  path, include

from webapp.views import IntroViewList
from webapp.views.quiz_view import QuizCreateView

urlpatterns = [
    path('', IntroViewList.as_view(), name='intro'),
    path('quiz/create/', QuizCreateView.as_view(), name='quiz_create')
]

app_name = 'webapp'
