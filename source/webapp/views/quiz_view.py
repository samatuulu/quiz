from django.urls import reverse
from django.views.generic import CreateView

from webapp.forms import QuizCreateForm
from webapp.models import Quiz


class QuizCreateView(CreateView):
    model = Quiz
    template_name = 'webapp/quiz/quiz_create.html'
    form_class = QuizCreateForm

    def form_valid(self, form):
        if str(self.request.user) != 'AnonymousUser':
            form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:intro')