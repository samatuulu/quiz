from django.views.generic import ListView

from webapp.models import Intro, Quiz


class IntroViewList(ListView):
    model = Intro
    context_object_name = 'post_intro'
    template_name = 'webapp/intro/intro_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['quizes'] = Quiz.objects.all()
        return context
