from django.db import models


class Intro(models.Model):
    title = models.CharField(max_length=250, verbose_name='Intro title')
    author = models.ForeignKey('auth.User', related_name='author_intro', on_delete=models.SET_NULL,
                               null=True, blank=True, verbose_name='Quiz Author')
    author_link = models.CharField(max_length=150, null=True, blank=True, verbose_name='Author Link')
    description = models.TextField(max_length=2000, null=True, blank=True, verbose_name='Intro description')
    image = models.ImageField(upload_to='intro_images', null=True, blank=True, verbose_name='Intro image')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Date Created')

    def __str__(self):
        return self.author.username

    class Meta:
        verbose_name = 'Intro'
        verbose_name_plural = 'Intro'


class Quiz(models.Model):
    author = models.ForeignKey('auth.User', related_name='question_author', on_delete=models.PROTECT, verbose_name='Author')
    question = models.CharField(max_length=250, verbose_name='Question')
    image = models.ImageField(upload_to='question_images', null=True, blank=True, verbose_name='Question photo')
    question_hint = models.TextField(max_length=2000, verbose_name='Question hint')

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Quiz'
        verbose_name_plural = 'Quizes'


class QuizOptions(models.Model):
    quiz = models.ForeignKey('webapp.Quiz', related_name='options', on_delete=models.CASCADE, verbose_name='Question')
    user_quiz_answer= models.TextField(max_length=200, verbose_name='Options')

    def __str__(self):
        return self.user_quiz_answer

    class Meta:
        verbose_name = 'Quiz Option'
        verbose_name_plural = 'Quiz Options'
